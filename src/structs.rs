use std::{collections::HashMap, path::PathBuf};

#[derive(Debug)]
pub struct Config {
	root:  PathBuf,
	inner: ParsedConfig,
}

impl Config {
	pub fn from_parsed(parsed: ParsedConfig, root: PathBuf) -> Self { Self { root, inner: parsed } }

	pub fn extensions(&self) -> &[String] { &self.inner.extensions }

	fn make_path_root(&self, s: &str) -> PathBuf { self.root.join(s) }

	pub fn resolve_preset_path(&self, name: &str) -> Option<PathBuf> {
		let pth = self.make_preset_path(name);

		if pth.exists() {
			return Some(pth);
		}

		None
	}

	pub fn make_meta_path(&self) -> PathBuf { self.make_path_root("meta.json") }

	pub fn make_preset_path(&self, name: &str) -> PathBuf {
		self.make_path_root(&self.inner.presets).join(format!("{}.json", name))
	}

	pub fn make_type_path(&self, typ: &str) -> Option<PathBuf> {
		if let Some(real_type) = self.inner.aliases.get(typ) {
			return self.inner.types.get(real_type).map(|v| self.make_path_root(&v.location));
		}
		self.inner.types.get(typ).map(|v| self.make_path_root(&v.location))
	}
}

#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub struct ParsedConfig {
	pub presets:    String,
	pub extensions: Vec<String>,
	pub types:      HashMap<String, TypeConfig>,
	pub aliases:    HashMap<String, String>,
}

#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub struct TypeConfig {
	pub location: String,
}

#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub struct Pack {
	pub meta:     PackMetadata,
	pub preset:   String,
	pub external: Vec<PackExternal>,
}

#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub struct PackMetadata {
	pub name:        String,
	pub version:     u32,
	pub author:      String,
	pub description: String,
	pub keysight:    String,

	#[serde(flatten)]
	additional: HashMap<String, serde_json::Value>,
}

#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub struct PackExternal {
	#[serde(rename = "type")]
	pub typ:       String,
	pub name:      String,
	pub extension: Option<String>,
}

#[derive(Debug, serde::Serialize)]
pub struct KeysightResult {
	pub status:       KeysightStatus,
	pub message:      String,
	pub message_full: String,
}

impl KeysightResult {
	pub fn new(status: KeysightStatus, message: String, message_full: String) -> Self {
		Self { status, message, message_full }
	}

	pub fn new_long(status: KeysightStatus, message_full: String) -> Self {
		let message = if message_full.len() > 80 {
			String::from(&message_full[..80])
		} else {
			message_full.clone()
		};
		Self { status, message, message_full }
	}
}

#[derive(Debug, serde::Serialize)]
#[serde(rename = "snake_case")]
pub enum KeysightStatus {
	#[serde(rename = "success")]
	Success,
	#[serde(rename = "error")]
	Error,
}
