use core::fmt;

use crate::structs::{Config, PackMetadata};

pub mod pack;
pub mod unpack;

pub const META_FILE_NAME: &str = "meta.json";
pub const PRESET_FILE_NAME: &str = "preset.json";
pub const ASSETS_DIR_NAME: &str = "assets";

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct PackAsset {
	pub hash: String,
	pub typ:  String,
	pub name: String,
	pub ext:  String,
}

impl fmt::Display for PackAsset {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		write!(f, "Asset[{}.{} :{} ({})]", self.name, self.ext, self.typ, self.hash)
	}
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct Package {
	pub meta:   PackMetadata,
	pub assets: Vec<PackAsset>,
}

#[derive(Debug, thiserror::Error, miette::Diagnostic)]
#[error("validation error on {}: {} (Asset = {})", .target, .message, .asset)]
pub struct ValidationError {
	target:  &'static str,
	message: &'static str,
	asset:   String,
}

impl Package {
	pub fn validate(mut self, _config: &Config) -> Result<Self, ValidationError> {
		self.meta.name =
			self.meta.name.chars().filter(|c| !matches!(c, '\\' | '/' | '.')).collect();
		for asset in &mut self.assets {
			if asset.hash.bytes().any(|v| !matches!(v, b'a'..=b'z' | b'0'..=b'9')) {
				return Err(ValidationError {
					target:  "hash",
					message: "invalid hash for asset",
					asset:   format!("{}", asset),
				});
			}

			asset.name = asset.name.chars().filter(|c| !matches!(c, '\\' | '/' | '.')).collect();
			asset.ext = asset.ext.chars().filter(|c| !matches!(c, '\\' | '/' | '.')).collect();

            if !_config.extensions().iter().any(|ext| *ext == asset.ext) {
                return Err(ValidationError {
                    target: "ext",
                    message: "invalid extension for asset",
                    asset: format!("{}", asset)
                })
            }
		}

		Ok(self)
	}
}
