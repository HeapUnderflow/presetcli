use std::{
	collections::BTreeSet,
	fs::File,
	io::{self, BufWriter, Read, Write},
	path::{Path, PathBuf},
};

use crate::structs::{Config, Pack};

use super::{PackAsset, Package, ASSETS_DIR_NAME, META_FILE_NAME, PRESET_FILE_NAME};

#[derive(Debug, thiserror::Error, miette::Diagnostic)]
pub enum Error {
	#[error("io error")]
	#[diagnostic(code(pack::io))]
	IO(#[source] io::Error),

	#[error("zip error")]
	#[diagnostic(code(pack::zip))]
	Zip(#[source] zip::result::ZipError),

	#[error("missing type root: {}", .0)]
	#[diagnostic(code(pack::missing_typeroot))]
	MissingTyperoot(String),

	#[error("missing asset file: ({}) {}", .typ, .name)]
	#[diagnostic(code(pack::missing_asset))]
	MissingAsset { typ: String, name: String },

	#[error("missing preset file: {}", .name)]
	#[diagnostic(code(pack::missing_preset))]
	MissingPreset { name: String },

	#[error("failed serializing data")]
	#[diagnostic(code(pack::json))]
	Serialize(#[source] serde_json::Error),
}

impl Into<crate::structs::KeysightResult> for Error {
	fn into(self) -> crate::structs::KeysightResult {
		let msg = format!("{:?}", self);
		crate::structs::KeysightResult::new_long(crate::structs::KeysightStatus::Error, msg)
	}
}

fn find_file_with_ext<'ex>(
	folder: &Path,
	exts: &'ex [String],
	filename: &str,
) -> Option<(&'ex str, PathBuf)> {
	for ext in exts {
		let p = folder.join(format!("{}.{}", filename, ext));
		if p.exists() {
			return Some((ext, p));
		}
	}

	None
}

#[instrument(skip(pack_config, preset_config))]
pub fn pack(
	target: impl AsRef<Path> + std::fmt::Debug,
	pack_config: &Config,
	preset_config: Pack,
) -> Result<(), Error> {
	_pack(target.as_ref(), pack_config, preset_config)
}

fn _pack(target: &Path, pack_config: &Config, preset_config: Pack) -> Result<(), Error> {
	let preset_path = pack_config
		.resolve_preset_path(&preset_config.preset)
		.ok_or_else(|| Error::MissingPreset { name: preset_config.preset.clone() })?;

	let mut output = BufWriter::new(File::create(target).map_err(Error::IO)?);
	let mut zipfile = zip::write::ZipWriter::new(&mut output);

	let zipoptions = zip::write::FileOptions::default()
		.compression_method(zip::CompressionMethod::Zstd)
		.compression_level(Some(19))
		.large_file(false);

	let mut hashes_written: BTreeSet<[u8; blake3::OUT_LEN]> = BTreeSet::new();
	let mut asset_entries = Vec::new();

	zipfile.add_directory(ASSETS_DIR_NAME, zipoptions).map_err(Error::Zip)?;

	for asset in preset_config.external {
		let type_root = pack_config
			.make_type_path(&asset.typ)
			.ok_or_else(|| Error::MissingTyperoot(asset.typ.clone()))?;

		let ext;
		let file_src = match asset.extension {
			Some(v) => {
				ext = v.clone();
				type_root.join(format!("{}.{}", asset.name, v))
			},
			None => {
				let (ex, path) =
					find_file_with_ext(type_root.as_path(), pack_config.extensions(), &asset.name)
						.ok_or_else(|| Error::MissingAsset {
							typ:  asset.typ.clone(),
							name: asset.name.clone(),
						})?;

				ext = ex.to_string();

				path
			},
		};

		let mut src = File::open(&file_src).map_err(Error::IO)?;

		// used as a size hint for the buffer vec
		let file_size = src.metadata().map(|v| v.len()).unwrap_or(1024 * 64);

		let mut full_file_buffer: Vec<u8> = Vec::with_capacity(file_size as usize);
		let mut read_buffer = [0u8; 1024 * 64];
		let mut hasher = blake3::Hasher::new();

		loop {
			let read = src.read(&mut read_buffer).map_err(Error::IO)?;

			if read == 0 {
				break;
			}

			hasher.update(&read_buffer[..read]);
			full_file_buffer.extend(&read_buffer[..read]);
		}

		let hash = hasher.finalize();

		if hashes_written.contains(hash.as_bytes()) {
			info!(%hash, "already wrote this hash");
			continue;
		} else {
			debug!(%hash, "wrote hash");
		}

		hashes_written.insert(hash.into());

		zipfile
			.start_file(format!("{}/{}", ASSETS_DIR_NAME, hash.to_hex()), zipoptions)
			.map_err(Error::Zip)?;
		zipfile.write_all(&full_file_buffer).map_err(Error::IO)?;

		asset_entries.push(PackAsset {
			hash: format!("{}", hash),
			typ: asset.typ,
			name: asset.name,
			ext,
		});
	}

	let mut preset_file = File::open(preset_path).map_err(Error::IO)?;
	zipfile.start_file(PRESET_FILE_NAME, zipoptions).map_err(Error::Zip)?;
	std::io::copy(&mut preset_file, &mut zipfile).map_err(Error::IO)?;

	zipfile.start_file(META_FILE_NAME, zipoptions).map_err(Error::Zip)?;

	serde_json::to_writer(&mut zipfile, &Package {
		meta:   preset_config.meta,
		assets: asset_entries,
	})
	.map_err(Error::Serialize)?;

	Ok(())
}
