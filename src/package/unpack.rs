use std::{
	fs::File,
	io,
	path::Path,
};
use zip::ZipArchive;

use crate::{
	package::{Package, META_FILE_NAME, PRESET_FILE_NAME, ASSETS_DIR_NAME},
	structs::Config,
};

use super::{PackAsset, ValidationError};

const MAX_UNPACK_SIZE_JSON: usize = 1024 * 1024;
const MAX_UNPACK_SIZE_ASSET: usize = 25 * 1024 * 1024;

#[derive(Debug, thiserror::Error, miette::Diagnostic)]
pub enum Error {
	#[error("io error")]
	#[diagnostic(code(unpack::io))]
	IO(#[source] io::Error),

	#[error("zip error")]
	#[diagnostic(code(unpack::zip))]
	Zip(#[source] zip::result::ZipError),

	#[error("malformed json error")]
	#[diagnostic(code(unpack::json))]
	Json(#[source] serde_json::Error),

	#[error("missing source file")]
	#[diagnostic(code(unpack::missing_source))]
	MissingSource,

	#[error("type {} is invalid", .0)]
	#[diagnostic(code(unpack::invalid_type))]
	InvalidType(String),

	#[error("asset is too large")]
	TooLarge,

	#[error("validation failed")]
	InvalidPackage(#[source] ValidationError),
}

struct MaxRead<R> {
	read:  usize,
	max:   usize,
	inner: R,
}

impl<R> MaxRead<R>
where
	R: io::Read,
{
	fn new(f: R, max: usize) -> Self { Self { read: 0, max, inner: f } }
}

impl<R> io::Read for MaxRead<R>
where
	R: io::Read,
{
	fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
		let r = self.inner.read(buf)?;
		self.read += r;
		if self.read > self.max {
			return io::Result::Err(io::Error::new(
				io::ErrorKind::FileTooLarge,
				format!("file exeeded the size of {} bytes", self.max),
			));
		}
		Ok(r)
	}
}

macro_rules! read_protect {
	(json: $ri:expr) => {
		MaxRead::new($ri, MAX_UNPACK_SIZE_JSON)
	};
	(asset: $ri:expr) => {
		MaxRead::new($ri, MAX_UNPACK_SIZE_ASSET)
	};
}

macro_rules! ensure_dir {
    ($dir:expr) => {{
        if !$dir.exists() {
            debug!(target=%$dir.display(), "creating nonexisting dir");
            std::fs::create_dir_all($dir).map_err(Error::IO)?;
        }
    }}
}

type UnpackResult<R> = Result<R, Error>;
type Archive = zip::ZipArchive<File>;

pub fn unpack(config: &Config, source: impl AsRef<Path> + std::fmt::Debug) -> UnpackResult<()> {
	_unpack(config, source.as_ref())
}

fn _unpack(config: &Config, source: &Path) -> UnpackResult<()> {
	if !source.exists() {
		return Err(Error::MissingSource);
	}

	let source_file = File::open(source).map_err(Error::IO)?;
	let mut zipfile = ZipArchive::new(source_file).map_err(Error::Zip)?;

	let package_metadata: Package = load_package_metadata(&mut zipfile, config)?;
	write_meta_file(&package_metadata, config)?;
	unpack_preset_file(&mut zipfile, config, &package_metadata)?;

	for asset in package_metadata.assets {
		unpack_asset_file(&mut zipfile, config, &asset)?;
	}

	Ok(())
}

fn load_package_metadata(zipfile: &mut Archive, config: &Config) -> UnpackResult<Package> {
	let meta_file = read_protect!(json: zipfile.by_name(META_FILE_NAME).map_err(Error::Zip)?);
	let meta: Package = serde_json::from_reader(meta_file).map_err(Error::Json)?;
	meta.validate(config).map_err(Error::InvalidPackage)
}

fn write_meta_file(metadata: &Package, config: &Config) -> UnpackResult<()> {
	let meta_target = config.make_meta_path();

	ensure_dir!(meta_target.parent().unwrap());

	let meta_file = File::create(meta_target).map_err(Error::IO)?;
	serde_json::to_writer(meta_file, metadata).map_err(Error::Json)?;
	Ok(())
}

fn unpack_preset_file(
	zipfile: &mut Archive,
	config: &Config,
	package_metadata: &Package,
) -> UnpackResult<()> {
	let mut preset_file =
		read_protect!(json: zipfile.by_name(PRESET_FILE_NAME).map_err(Error::Zip)?);
	let preset_target = config.make_preset_path(&package_metadata.meta.name);

	ensure_dir!(preset_target.parent().unwrap());
	debug!(target=%preset_target.display(), "unpacking preset");

	let mut preset_out = File::create(preset_target).map_err(Error::IO)?;
	io::copy(&mut preset_file, &mut preset_out).map_err(Error::IO)?;
	Ok(())
}

fn unpack_asset_file(
	zipfile: &mut Archive,
	config: &Config,
	asset: &PackAsset,
) -> UnpackResult<()> {
	let mut asset_file = read_protect!(asset: zipfile.by_name(&format!("{}/{}", ASSETS_DIR_NAME, asset.hash)).map_err(Error::Zip)?);
	let asset_type_path =
		config.make_type_path(&asset.typ).ok_or_else(|| Error::InvalidType(asset.typ.clone()))?;

	ensure_dir!(&asset_type_path);

	let asset_target = asset_type_path.join(format!("{}.{}", asset.name, asset.ext));

	debug!(target=%asset_target.display(), "unpacking asset");

	let mut asset_out = File::create(asset_target).map_err(Error::IO)?;
	io::copy(&mut asset_file, &mut asset_out).map_err(Error::IO)?;
	Ok(())
}
