#![feature(io_error_more)]
#[macro_use]
extern crate tracing;

use std::{
	fs::{self, File},
	path::PathBuf,
};

use clap::Parser;
use miette::{Context, IntoDiagnostic};
use structs::{KeysightResult, Pack};
use tracing_subscriber::EnvFilter;

use crate::structs::Config;

mod package;
mod structs;

type ProgramResult = Result<(), ProgramError>;

fn main() -> miette::Result<()> {
	let args = Args::parse();

	#[cfg(debug_assertions)]
	{
		std::env::set_var("PRESETCLI_LOG", concat!("info,", env!("CARGO_PKG_NAME"), "=trace"));
	}

	tracing_subscriber::fmt()
		.with_env_filter(EnvFilter::from_env("PRESETCLI_LOG"))
		.with_thread_names(true)
		.init();

	let report = args.report.clone();
	let res = program(args);

	if let Some(parent) = report.parent() {
		if !parent.exists() {
			fs::create_dir_all(parent)
				.into_diagnostic()
				.wrap_err("failed to ensure report parent dir exists")?;
		}
	}
	let report_f =
		File::create(report).into_diagnostic().wrap_err("failed to create report json")?;

	if let Err(ref why) = &res {
		let mut rendered = String::new();
		miette::GraphicalReportHandler::new_themed(miette::GraphicalTheme::unicode_nocolor())
			.with_width(80)
			.tab_width(4)
			.render_report(&mut rendered, why)
			.expect("hard failure: miette failed to render report");

		let res =
			format!("---- Encountered error: ----\n{}\n\n---- Details: ----\n{:#?}", rendered, why);

		serde_json::to_writer(
			report_f,
			&KeysightResult::new(structs::KeysightStatus::Error, format!("{}", why), res),
		)
		.into_diagnostic()
		.wrap_err("failed to serialize error json to file")?;
	} else {
		serde_json::to_writer(
			report_f,
			&KeysightResult::new_long(
				structs::KeysightStatus::Success,
				"successfully completed action".to_owned(),
			),
		)
		.into_diagnostic()
		.wrap_err("unable to write success json")?;
	}

	res.wrap_err("program error")
}

#[allow(clippy::upper_case_acronyms)]
#[derive(Debug, thiserror::Error, miette::Diagnostic)]
enum ProgramError {
	#[error("io: {}", .description)]
	IO {
		#[source]
		source:      std::io::Error,
		description: &'static str,
	},
	#[error("io: {}", .description)]
	JSON {
		#[source]
		source:      serde_json::Error,
		description: &'static str,
	},
	#[error("pack: {}", .description)]
	Pack {
		#[source]
		source:      package::pack::Error,
		description: &'static str,
	},
	#[error("unpack: {}", .description)]
	Unpack {
		#[source]
		source:      package::unpack::Error,
		description: &'static str,
	},
	#[error("package file not found")]
	PackageNotFound,
}

macro_rules! mapdesc {
	(IO, $desc:expr) => {{
		|__value| ProgramError::IO { source: __value, description: $desc }
	}};
	(JSON, $desc:expr) => {{
		|__value| ProgramError::JSON { source: __value, description: $desc }
	}};
	(Pack, $desc:expr) => {{
		|__value| ProgramError::Pack { source: __value, description: $desc }
	}};
	(Unpack, $desc:expr) => {{
		|__value| ProgramError::Unpack { source: __value, description: $desc }
	}};
}

fn program(args: Args) -> ProgramResult {
	debug!("verb: {:?}", args);

	let config = {
		let cfgf = File::open(args.config).map_err(mapdesc!(IO, "unable to open config file"))?;
		let cfg =
			serde_json::from_reader(cfgf).map_err(mapdesc!(JSON, "unable to parse config"))?;

		Config::from_parsed(cfg, args.root.clone())
	};

	match args.command {
		Command::Pack(pack_args) => command_pack(config, pack_args),
		Command::Unpack(unpack_args) => command_unpack(config, unpack_args),
	}
}

fn command_unpack(config: Config, unpack_args: UnpackCommandArgs) -> ProgramResult {
	debug!(?unpack_args, "unpacking");

	if !unpack_args.source.exists() {
		return Err(ProgramError::PackageNotFound);
	}

	package::unpack::unpack(&config, unpack_args.source)
		.map_err(mapdesc!(Unpack, "fauiled to unpack preset"))?;

	Ok(())
}

fn command_pack(config: Config, pack_args: PackCommandArgs) -> ProgramResult {
	debug!(?pack_args, "loading packing info");
	let pack: Pack = {
		let packf = File::open(pack_args.options)
			.map_err(mapdesc!(IO, "unable to open package info file"))?;
		serde_json::from_reader(packf)
			.map_err(mapdesc!(JSON, "unable to parse package info file"))?
	};

	error_span!("pack").in_scope(|| {
		debug!(info=?pack, "packing using pack-info");
		package::pack::pack(pack_args.output_path, &config, pack)
			.map_err(mapdesc!(Pack, "failed to pack preset"))
	})
}

#[derive(Debug, clap::Parser)]
struct Args {
	#[clap(short, long)]
	/// Configuration file used for packing
	config: PathBuf,

	#[clap(short, long)]
	/// Target root
	root: PathBuf,

	#[clap(long)]
	report: PathBuf,

	/// Action to perform
	#[command(subcommand)]
	command: Command,
}

#[derive(Debug, clap::Subcommand)]
enum Command {
	/// Pack the given preset into an archive
	Pack(PackCommandArgs),
	/// Unpack the given preset to a keysight directory
	Unpack(UnpackCommandArgs),
}

#[derive(Debug, clap::Args)]
struct PackCommandArgs {
	#[clap(value_name = "OPTIONS_PATH")]
	pub options:     PathBuf,
	pub output_path: PathBuf,
}

#[derive(Debug, clap::Args)]
struct UnpackCommandArgs {
	pub source: PathBuf,
}
