{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";

    fenix = {
      url = "github:nix-community/fenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, flake-utils, fenix, nixpkgs, ... } @ inputs:
    flake-utils.lib.eachDefaultSystem (system:
      let
        overlays = [ fenix.overlays.default ];
        pkgs = import nixpkgs { inherit system overlays; };
        toolchain = pkgs.fenix.fromToolchainFile {
          file = ./rust-toolchain.toml;
          sha256 = "sha256-mVggTSi0AlRQpVUvQHbcxC/ZBarWgkUrMd13UkOQvTU=";
        };

        buildInputs = [ toolchain ] ++ (with pkgs; [
          openssl
          pkg-config
        ]);

        nativeBuildInputs = [];
        ldLibraryPath = pkgs.lib.makeLibraryPath buildInputs;
      in
        {
          devShell = pkgs.mkShell {
            buildInputs = buildInputs ++ [ pkgs.rust-analyzer-nightly ];
            nativeBuildInputs = nativeBuildInputs;
            shellHook = ''
            export LD_LIBRARY_PATH="${ldLibraryPath}:$LD_LIBRARY_PATH"
            '';
          };
        });
}
